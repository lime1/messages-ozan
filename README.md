Features

- ✅ Display all users full name
- ✅ Allow for flagging a conversation (via toggling star icon)
- ✅ Allow searching to filter based on the full name
- ✅ Allow clicking on any user and showing a basic chat environment with the information stored in messages
- ✅ Only one 3rd party was used. It is the "faker" to generate messages datas.
- ✅ Used Context API
- ✅ Used SCSS themes and functions rem-calc
-

Additional features

- ✅ Messages UI implementation and generate messages when click the user randomly.
- ✅ Deployed to Vercel -> https://messages-ozan.vercel.app/

In the project directory, you can run:

### `yarn start`

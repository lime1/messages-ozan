import { useRef, useEffect, useCallback } from "react";

export function useDebouncedCall(
  cb: (...args: any[]) => void,
  time: number,
  deps: readonly any[],
  argsFn?: (...args: any[]) => any[]
) {
  const timeout = useRef<NodeJS.Timeout>();
  const caller = useCallback(
    (...args) => {
      timeout.current && clearTimeout(timeout.current);
      const params = argsFn && argsFn(...args);
      timeout.current = setTimeout(() => {
        cb(...(params || args));
      }, time);
    },
    [...deps, time]
  );

  useEffect(() => {
    return () => {
      timeout.current && clearTimeout(timeout.current);
    };
  }, []);

  return caller;
}

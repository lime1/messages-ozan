import React, { FunctionComponent, Suspense, useCallback } from "react";
import { Contact } from "../context/AppContextReducer";
import { StarButton } from "./StarButton";

const AvatarComponent = React.lazy(() => import("./Avatar"));

type ContactBoxProps = {
  contact: Contact;
  isSelected: boolean;
  toggleFavorite: (id: number) => void;
  onClickContact: (id: number) => void;
};

const ContactBox: FunctionComponent<ContactBoxProps> = ({
  contact: { id, fullName, lastMessage, isFavorite, avatar },
  toggleFavorite,
  onClickContact,
  isSelected,
}) => {
  const onClickStarButton = (
    e: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    e.stopPropagation();
    if (id) {
      toggleFavorite(id);
    }
  };
  const onClickBox = useCallback(() => {
    onClickContact(id);
  }, [id, onClickContact]);

  return (
    <div
      className={"contact-box" + " contact-box__selected--" + !!isSelected}
      onClick={onClickBox}
    >
      <div className="contact-box__avatar">
        <Suspense fallback="">
          <AvatarComponent source={avatar} />
        </Suspense>
      </div>

      <div className="contact-box__section">
        <div className="contact-box__header">
          <h3 className="contact-box__title">{fullName}</h3>
          <span className="contact-box__time">1h</span>
        </div>
        <div className="contact-box__message">
          <span className="contact-box__text">{lastMessage}</span>
          <span className="contact-box__star" onClick={onClickStarButton}>
            <StarButton isFavorite={isFavorite} />
          </span>
        </div>
      </div>
    </div>
  );
};

export default ContactBox;

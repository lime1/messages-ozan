import React from "react";
import { ReactComponent as SvgChatBubble } from "../assets/chat.svg";

export const NewChatButton = () => {
  return (
    <div className="new-chat-button">
      <button>
        <SvgChatBubble />
      </button>
    </div>
  );
};

import React from "react";

export const ChatInputBox = () => {
  return (
    <div className="chat-input-box">
      <div className="chat-input-box__input">
        <input type="text" placeholder="Type a message" />
      </div>
    </div>
  );
};

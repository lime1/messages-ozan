import React, { FunctionComponent, useState } from "react";
import { useDebouncedCall } from "../hooks/useDebouncedCall";

type SearchProps = { onChange: (text: string) => void };

export const Search: FunctionComponent<SearchProps> = (props) => {
  const [text, setText] = useState("");

  const onChange = useDebouncedCall(
    (e) => {
      setText(e.target.value);
      props.onChange(e.target.value);
    },
    200,
    [text]
  );

  return (
    <div className="search">
      <input
        type="text"
        placeholder="Search or a new chat"
        onChange={onChange}
      />
    </div>
  );
};

import React, { FunctionComponent } from "react";
import { Message } from "../context";

type MessageProps = {
  message: Message;
};

export const MessageBox: FunctionComponent<MessageProps> = ({ message }) => {
  return (
    <div className={`message-box ${message.isMainUser ? "sent" : "received"}`}>
      {message.message}
      <div className="message-box__metadata">
        <span className="message-box__date">
          {message.date.toLocaleString()}
        </span>
        {/* {message.isMainUser && (
          <img src={doubleCheck} alt="" className="icon-small" />
        )} */}
      </div>
    </div>
  );
};

import React, { FunctionComponent } from "react";
import SvgStar from "../assets/SvgStar";

type StarButtonProps = {
  isFavorite?: boolean;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
};

export const StarButton: FunctionComponent<StarButtonProps> = ({
  isFavorite,
  onClick,
}) => {
  return (
    <div
      className={"star-button" + " star-button--active--" + !!isFavorite}
      onClick={onClick}
    >
      <SvgStar width={20} />
    </div>
  );
};

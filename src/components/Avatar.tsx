import React, { FunctionComponent } from "react";

type AvatarProps = {
  name?: string;
  source: string | undefined;
};

const Avatar: FunctionComponent<AvatarProps> = ({ name, source }) => {
  return (
    <div className="avatar">
      <img className="avatar__image" src={source} alt="" />
      {name && <div className="avatar__name">{name}</div>}
    </div>
  );
};

export default Avatar;

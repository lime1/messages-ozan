import { createContext, Dispatch } from 'react';
import { appContextInitialStates, IAppContextState } from './AppContextReducer';
import { IAppContextActionTypes } from './AppContextActions';

export interface IAppContext {
  contextState: IAppContextState;
  contextDispatch: Dispatch<IAppContextActionTypes>;
}

export const appContext = createContext<IAppContext>({
  contextState: appContextInitialStates,
  contextDispatch: () => {},
});

export const AppContextProvider = appContext.Provider;

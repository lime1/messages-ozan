import faker from "faker";
import { truncate } from "../helpers/truncate";
import { IAppContextActionTypes } from "./AppContextActions";

export type Message = {
  id: number;
  message: string;
  isMainUser: boolean;
  date: Date;
};

export type Messages = {
  contact?: Contact;
  messageData: Message[];
};

export type Contact = {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  isFavorite: boolean;
  lastMessage?: string;
  fullName?: string;
  avatar?: string;
};

export interface IAppContextState {
  contacts: Contact[];
  messages: Messages;
  searchText: string;
}

export const appContextInitialStates: IAppContextState = {
  contacts: [],
  messages: { contact: undefined, messageData: [] },
  searchText: "",
};

export const appContextReducer = (
  state: IAppContextState,
  action: IAppContextActionTypes
): IAppContextState => {
  switch (action.type) {
    case "SET_CONTACTS_DATA": {
      return {
        ...state,
        contacts: action.contacts.map((item) => {
          return {
            ...item,
            lastMessage: truncate(
              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum suscipit quisquam culpa sapiente repellendus impedit sint repudiandae? Illo, sed. Vero, iure quas ipsam dolor veniam nemo aliquam dolorem mollitia error?",
              55
            ),
            fullName: `${item.firstName} ${item.lastName}`,
            avatar: faker.internet.avatar(),
          };
        }),
      };
    }
    case "SET_MESSAGES_DATA": {
      const contact = state.contacts.find(
        (element) => element.id === action.contactId
      );

      return {
        ...state,
        messages: { contact, messageData: action.messages },
      };
    }
    case "TOGGLE_FAVORITE": {
      const contactIndex = state.contacts.findIndex(
        (element) => element.id === action.id
      );

      let newContacts = [...state.contacts];
      newContacts[contactIndex] = {
        ...newContacts[contactIndex],
        isFavorite: !newContacts[contactIndex].isFavorite,
      };

      return {
        ...state,
        contacts: newContacts,
      };
    }

    case "SET_SEARCH_TEXT": {
      return {
        ...state,
        searchText: action.searchText,
      };
    }
    default:
      return state;
  }
};

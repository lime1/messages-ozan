import { Contact, Message } from "./AppContextReducer";

export type IAppContextActionTypes =
  | {
      type: "SET_CONTACTS_DATA";
      contacts: Contact[];
    }
  | {
      type: "TOGGLE_FAVORITE";
      id: number;
    }
  | {
      type: "SET_SEARCH_TEXT";
      searchText: string;
    }
  | {
      type: "SET_MESSAGES_DATA";
      contactId: number;
      messages: Message[];
    };

export const setContactsData = (
  contacts: Contact[]
): IAppContextActionTypes => ({
  type: "SET_CONTACTS_DATA",
  contacts,
});

export const toggleFavorite = (id: number): IAppContextActionTypes => ({
  type: "TOGGLE_FAVORITE",
  id,
});

export const setSearchText = (searchText: string): IAppContextActionTypes => ({
  type: "SET_SEARCH_TEXT",
  searchText,
});

export const setMessagesData = (
  contactId: number,
  messages: Message[]
): IAppContextActionTypes => ({
  type: "SET_MESSAGES_DATA",
  contactId,
  messages,
});

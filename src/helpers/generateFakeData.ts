import faker from "faker";
import { Message } from "../context";

export const generateMessagesData = (): Message[] => {
  const arr = new Array(faker.random.number({ min: 1, max: 50 })).fill("");
  const generatedList: Message[] = arr.map((i, index) => ({
    id: faker.random.number(6),
    message: faker.lorem.words(),
    date: faker.date.recent(),
    isMainUser: (index + 1) % 2 === 0,
  }));

  return generatedList;
};

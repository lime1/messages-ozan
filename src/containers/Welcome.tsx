import React from "react";

export const Welcome = () => {
  return (
    <div className="welcome">
      <h1>Select a conversation</h1>
      <h3>
        Start by selecting a conversation or searching for someone specific
      </h3>
    </div>
  );
};

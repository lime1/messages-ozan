import React, { FunctionComponent, useContext } from "react";
import ContactBox from "../components/ContactBox";
import {
  appContext,
  Contact,
  setMessagesData,
  toggleFavorite,
} from "../context";
import { generateMessagesData } from "../helpers/generateFakeData";

type ContactsProps = {
  contacts: Contact[];
};

export const Contacts: FunctionComponent<ContactsProps> = ({ contacts }) => {
  const { contextDispatch, contextState } = useContext(appContext);

  const onChangeFavorite = (id: number) => {
    contextDispatch(toggleFavorite(id));
  };

  const onClickContactBox = (id: number) => {
    const messagesData = generateMessagesData();

    contextDispatch(setMessagesData(id, messagesData));
  };

  const selectedContactId = contextState.messages.contact?.id;

  return (
    <div className="contacts">
      {contacts.map((contact) => (
        <ContactBox
          contact={contact}
          key={contact.id}
          toggleFavorite={onChangeFavorite}
          onClickContact={onClickContactBox}
          isSelected={selectedContactId === contact.id}
        />
      ))}
    </div>
  );
};

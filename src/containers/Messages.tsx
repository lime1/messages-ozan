import React, { FunctionComponent, useEffect, useRef } from "react";
import { Contact, Message } from "../context";
import { MessageBox } from "../components/MessageBox";
import Avatar from "../components/Avatar";
import { ChatInputBox } from "../components/ChatInputBox";

type MessagesProps = {
  contact?: Contact;
  messagesData: Message[];
};

export const Messages: FunctionComponent<MessagesProps> = ({
  messagesData,
  contact,
}) => {
  const endDiv = useRef<HTMLDivElement>(null);
  useEffect(() => {
    endDiv?.current?.scrollIntoView();
  }, [messagesData]);

  return (
    <div className="messages">
      <header>
        <Avatar name={contact?.fullName} source={contact?.avatar} />
      </header>
      <div className="messages__list">
        {messagesData
          .sort((a, b) => a.date.getTime() - b.date.getTime())
          .map((m, index) => (
            <MessageBox message={m} key={index} />
          ))}
        <div ref={endDiv}></div>
      </div>
      <div className="messages__input">
        <ChatInputBox />
      </div>
    </div>
  );
};

import React, { useContext } from "react";
import { FilterSelect } from "../components/FilterSelect";
import { FollowUpButton } from "../components/FollowUpButton";
import { NewChatButton } from "../components/NewChatButton";
import { Search } from "../components/Search";
import { appContext, setSearchText } from "../context";

export const ContactsHeader = () => {
  const { contextDispatch } = useContext(appContext);

  const onChangeSearch = (text: string) => {
    contextDispatch(setSearchText(text));
  };

  return (
    <div className="contacts-header">
      <div className="contacts-header__top">
        <Search onChange={onChangeSearch} />
        <NewChatButton />
      </div>
      <div className="contacts-header__bottom">
        <FilterSelect />
        <FollowUpButton />
      </div>
    </div>
  );
};

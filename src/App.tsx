import React, { useEffect, useMemo, useReducer } from "react";
import "./App.scss";
import { Contacts } from "./containers/Contacts";
import { ContactsHeader } from "./containers/ContactsHeader";
import { Messages } from "./containers/Messages";
import { Welcome } from "./containers/Welcome";
import {
  AppContextProvider,
  setContactsData,
  appContextInitialStates,
  appContextReducer,
} from "./context";

import contactsData from "./MOCK_DATA.json";

const App = () => {
  const [state, dispatch] = useReducer(
    appContextReducer,
    appContextInitialStates
  );
  const appContextProvider = useMemo(
    () => ({
      contextState: state,
      contextDispatch: dispatch,
    }),
    [state, dispatch]
  );

  useEffect(() => {
    dispatch(setContactsData(contactsData));
  }, []);

  const filteredContacts = useMemo(() => {
    if (!state.searchText) return state.contacts;
    return state.contacts.filter(({ fullName }) =>
      fullName?.toLowerCase().includes(state.searchText.toLocaleLowerCase())
    );
  }, [state.searchText, state.contacts]);

  return (
    <AppContextProvider value={appContextProvider}>
      <div className="app">
        <div className="app__left">
          <ContactsHeader />
          <Contacts contacts={filteredContacts} />
        </div>

        <div className="app__right">
          {state.messages.contact ? (
            <Messages
              messagesData={state.messages.messageData}
              contact={state.messages.contact}
            />
          ) : (
            <Welcome />
          )}
        </div>
      </div>
    </AppContextProvider>
  );
};

export default App;
